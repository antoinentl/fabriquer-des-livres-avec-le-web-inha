# Fabriquer des livres avec le Web
Support de présentation pour la conférence donnée à l'Institut national d'histoire de l'art dans le cadre du cycle de conférences des lundis du numérique : lundi 17 septembre 2018 à 18h à Paris ([détails en ligne](https://www.inha.fr/fr/agenda/parcourir-par-annee/en-2018/septembre-2018/les-lundis-numeriques-de-l-inha.html)).

## Support de présentation en ligne
Ce support de présentation est disponible en ligne :

[http://presentations.quaternum.net/fabriquer-des-livres-avec-le-web-inha/](http://presentations.quaternum.net/fabriquer-des-livres-avec-le-web-inha/)

## Outils utilisés pour réaliser ce support de présentation

- texte écrit avec le langage de balisage léger [AsciiDoc](https://asciidoctor.org/docs/what-is-asciidoc/), intégrant des diagrammes avec [asciidoctor-diagram](https://github.com/asciidoctor/asciidoctor-diagram/) ;
- version HTML générée avec [asciidoctor-revealjs](https://github.com/asciidoctor/asciidoctor-reveal.js), outil basé sur [Asciidoctor](https://asciidoctor.org/) et [reveal.js](https://github.com/hakimel/reveal.js) ;
- contenu versionné avec Git et la plateforme GitLab ;
- version HTML déployée avec [GitLab CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/).

## Modifications
Pour modifier ce support il faut intervenir sur le fichier `index.adoc`. Ce dépôt ne contient pas les dépendances liées à Asciidoctor, asciidoctor-diagram et asciidoctor-revealjs, il faut se référer à ces programmes pour pouvoir générer la version HTML.

Voici un exemple de ligne de commande permettant la génération de la version HTML :  
`bundle exec asciidoctor-revealjs -r asciidoctor-diagram index.adoc`


## Licence
Les contenus de cette présentation sont placés sous licence [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/).
